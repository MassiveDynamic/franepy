#!/usr/bin/env python
# -*- coding: utf-8 -*-
#ʹ
import re,os,csv,sys,folium,psycopg2,datetime,time,folium
from locate import *
from DB import *
from maps import *
from twython import Twython,TwythonStreamer   #Libreria per collegamento a Twitter
from folium import FeatureGroup, LayerControl, Map, Marker

path='/var/www/html/twitter/'#directory dell'intero progetto
TWITTER_APP_KEY = '' #supply the appropriate value
TWITTER_APP_KEY_SECRET = ''
TWITTER_ACCESS_TOKEN = ''
TWITTER_ACCESS_TOKEN_SECRET = ""
#Inizializzo le liste 
text=[]
date=[]
autore=[]
accuracy=[]
t=getToday()#tweet di oggi
now=[]
for x in t:
	now.append(x[0].replace("\n",""))
today=datetime.date.today() #acquisisco la data attuale

with open(path+"LOG/log0","w") as f: #Sovrascrivo all'avvio il file di log
	f.write("Tweet\n")
with open(path+"LOG/log1","w") as f:
	f.write("Tweet\n")
t = Twython(app_key=TWITTER_APP_KEY, 
	app_secret=TWITTER_APP_KEY_SECRET, 
	oauth_token=TWITTER_ACCESS_TOKEN, 
	oauth_token_secret=TWITTER_ACCESS_TOKEN_SECRET)

def get_tweet(): #funzione che estrapola i tweet , i dati correlati a loro (data, luogo, autore) e rimuove i caratteri speciali sostituendo successivamente gli apici con un accento greco
	global text,date,autore
	tweets=t.cursor(t.search,q='frana OR Frana -filter:retweets AND -filter:replies', count=100, lang='it')
	for tweet in tweets:
		text.append(' '+re.sub("[.;:@#,]",'',tweet['text']).replace("'","ʹ")+' ')
		date.append(tweet['created_at'].replace("'","ʹ"))
		autore.append(tweet['user']['name'].replace("'","ʹ"))


def log(i,text): #log su file di testo dei tweet giusti (log0) e sbagliati (log1) 
	with open(path+"LOG/Temp"+str(i),"a") as f:
		f.write(text.replace("\n","")+"\n\n>>")

def isReal(text,autore,date,lat,lon,accuratezza): #funzione che verifica se un tweet è vero o falso
	try:
		if int(date.split()[2])==int(today.day):
			save(text,autore,date,lat,lon,accuratezza)
		if accuratezza>0.66:
			log('Vero',text.replace("\n","")+str(accuratezza))
			return 'red'
		elif accuratezza>0.33:
			log('Indeciso',text.replace("\n","")+str(accuratezza))
			return 'orange'
		else:
			log('Falso',text.replace("\n","")+str(accuratezza))
			return 'gray'
	except:
		log('Indeciso',text.replace("\n",""))
		return 'gray'

def save(tuxt,autore,date,lat,lon,accuratezza):
	if tuxt.replace("\n","") not in now and text.count(tuxt)==1:
		conn=psycopg2.connect("dbname='???' user='???' host='???' password='???'")
		cur=conn.cursor()
		cur.execute("INSERT INTO tweetss (testo,lat,lon,data,accuracy,autore) VALUES( '"+tuxt+"',"+str(lat)+","+str(lon)+",'"+str(date)+"','"+str(accuratezza)+"','"+str(autore)+"');")
		cur.execute("COMMIT;")
		print(tuxt.replace("\n",""))
		conn.close()

def geolocate(text): #funzione che geolocalizza il tweet
	conn = psycopg2.connect(dbname="allcountries", password="postgres", user="postgres", host="194.119.218.92")
	lista=matchWords(text,conn)
	return lista

def main(di,file):
	try:
		gruppo_aff=FeatureGroup(name='Affidabile')
		gruppo_maybe=FeatureGroup(name='Incerto')
		gruppo_naff=FeatureGroup(name='Non affidabile')
		map_osm=sun()
		c=0
		for i in range(0,len(text)):
			if int(date[i].split()[2])==int(di):
				localita=geolocate(text[i])
				for loc in localita:
					try: 
						colore=isReal(text[i],autore[i],date[i],loc[0][1],loc[0][2],accuracy[i])
						if colore=='red':
							folium.Marker([loc[0][1],loc[0][2]],
								popup=(loc[0][0]+" Date ["+date[i]+'] \nFrom:[ '+autore[i]+' ] \n\n' +text[i]),
								icon=folium.Icon(color=colore, icon="warning-sign")).add_to(gruppo_aff)
						elif colore=='orange':
							folium.Marker([loc[0][1],loc[0][2]],
								popup=(loc[0][0]+" Date ["+date[i]+'] \nFrom:[ '+autore[i]+' ] \n\n' +text[i]),
								icon=folium.Icon(color=colore, icon="warning-sign")).add_to(gruppo_maybe)
						else:
							folium.Marker([loc[0][1],loc[0][2]],
								popup=(loc[0][0]+" Date ["+date[i]+'] \nFrom:[ '+autore[i]+' ] \n\n' +text[i]),
								icon=folium.Icon(color=colore, icon="warning-sign")).add_to(gruppo_naff)
						c=c+1
					except:
						pass #nel caso trovi una regione che di conseguenza non ha coordinat
					
		print('ho aggiunto',c,' marker nel giorno',di,'file: /var/www/html/twitter/mappe/',file)
		gruppo_aff.add_to(map_osm)
		gruppo_maybe.add_to(map_osm)
		gruppo_naff.add_to(map_osm)
		folium.LayerControl().add_to(map_osm)	
		map_osm.save(path+'mappe/'+file)
	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print('ERROR',exc_type, fname, exc_tb.tb_lineno)

def machineLearning():
	tweets=get_tweet() #acquisisco i tweet
	with open(path+"macchina/dataset/temp_dataset.csv","w") as f:
		for tweet in text:
			f.write(tweet.replace("\n","")+';\n')
	os.system("machine --modello frana --predici "+path+"macchina/dataset/temp_dataset.csv --output "+path+"macchina/dataset/temp_out.csv")
	with open(path+"macchina/dataset/temp_out.csv") as csvDataFile:
		csvReader = csv.reader(csvDataFile,delimiter=';')
		for row in csvReader:
			accuracy.append(float(row[2]))


machineLearning()
for i in range(0,10):
	main(int((today-datetime.timedelta(days=i)).day), str(i)+'d' )#processa le mappe


print("\n\n Sono stati processati [",len(text),"] Tweet")
