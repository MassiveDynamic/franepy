#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os, sys, re
import random
import codecs
from spacy.util import decaying

#~ Tag per la macchina: nome dell'indice di affidabilita
#~ e tag per il riconoscimento degli eventi

TAG = "positivo"
TAG_NER = "frana"

def wrong(t):
	return "\033[31;40m "+ t +"\033[00m"

def right(t):
	return "\033[32;40m "+ t +"\033[00m"

def toNote(t):
	return "\033[33;7m "+ t +"\033[00m"

# funzione della barra del progresso
def update_progress(p):#p = (num * 100) / totale
	b = ('\b'*100) + " [ " + toNote('{0}') + " ] {1}%"
	sys.stdout.write(b.format(' '*(p/2), p))
	sys.stdout.flush()



# Pulizia del testo
# Rimuovo link, virgolette ed espande le virgole
def sanitize(text):
	text = re.sub(r'http.*|pic.twitter.*|"', '', text)
	text = re.sub(r',', ', ', text)
	return text

# Normalizzazione della frase
# I verbi e ausiliari vengono lemmatizzati
def lemmatize(doc):
	text = ""
	for token in doc:
		if token.pos_ in ['VERB', 'AUX']:
			text += token.lemma_ + " "
		else:
			text += token.text + " "
	return text



# Carico i dati da un file
def load_data(dati, nlp):
	
	ttt = []
	
	for line in codecs.open(dati['training'], "r", "utf-8"):
		try:
			line = line.split(';')
			text = nlp(line[0])
			num = float(line[1])
			#start, end = line[2].split(',')[:]
		
			ttt.append((lemmatize(text), { 'cats': { TAG : num }}))
		
		#yield (
		#	lemmatize(text), 
		#	{ 'cats': { TAG : num },
		#		'entities': [ (int(start), int(end), TAG_NER) ]}
		#)
		
			
		except:
			pass
	return ttt


# Training del modello
def training(train_data, data, nlp):
	
	# Aggiunta della pipe 'textcat' per la classificazione dei testi
	if nlp.has_pipe('ner'):
		ner = nlp.get_pipe('ner')
	else:
		ner = nlp.create_pipe('ner')
		nlp.add_pipe(ner)
	ner.add_label(TAG_NER)

	# Aggiunta della pipe 'textcat' per la classificazione dei testi
	if nlp.has_pipe('textcat'):
		textcat = nlp.get_pipe('textcat')
	else:
		textcat = nlp.create_pipe('textcat')
		nlp.add_pipe(textcat)
	textcat.add_label(TAG)
		
	# Inizio training
	optimizer = nlp.begin_training()
	#optimizer.to_gpu()
	
	progress = 0
	length = len(train_data)
	dropout = decaying(0.6, 0.2, 0.05)
	
	n_iter = 1
	print("Allenamento: ",n_iter," iterazioni\n")
	
	for i in range (n_iter):
		random.shuffle(train_data)
		
		for text, annotation in train_data:
			nlp.update(
				[text],
				[annotation], 
				sgd=optimizer, 
				drop=next(dropout)
			)
			
			update_progress((progress * 100) / (length * n_iter)) 
			progress += 1

	update_progress(100)

	# Salvataggio modello
	nlp.to_disk(data['save'])
	
	print("\nTermine Allenamento\n")


def predici(dati, nlp):
	
	test_file		= codecs.open(dati['test'],		"r", "utf-8")
	output_file = codecs.open(dati['output'],	"w", "utf-8")
	scores = [] # valori assegnati e calcolati
	
	for line in test_file:
		line = line.split(';')
		
		line[0] = sanitize(line[0])
		cat = nlp(line[0]).cats[TAG]
		try:
			num = float(line[1])
		except:
			num = ''
		
		scores.append((num, cat))
		
		print (line[0] + "\n{}\t::->\t{}\n".format(num, cat))

		output_file.write(line[0] + "\n{}\t::->\t{:.5f}\n".format(num, cat))
		output_file.flush()

	return scores

def valuta(dati, scores):
	
	tp = 1e-8
	fp = 1e-8
	fn = 1e-8
	tn = 1e-8
	
	limite = 0.5
	
	for annotati, calcolati in scores:

		if annotati		>= limite and calcolati >= limite:
			tp += 1.
		elif annotati >= limite and calcolati <  limite:
			fp += 1.
		elif annotati <  limite and calcolati <  limite:
			tn += 1.
		elif annotati <  limite and calcolati >= limite:
			fn += 1.
	
	totale = len(scores)
	
	precisione = tp / (tp + fp)
	ratio_tp = tp / (tp + fn)
	ratio_tn = tn / (tn + fp)
	
	accuratezza = (tp + tn) / totale
	
	
	#f_score = 2 * (precision * recall) / (precision + recall)
	f = codecs.open(dati['tabella'], 'w')
	f.write("""
		Totale: {:d}
		
		Precisione: {:.3f}
		Ratio Veri Positivi: {:.3f}
		Ratio Veri Negativi: {:.3f}
		Accuratezza: {:.3f}

		""".format(totale, precisione, ratio_tp, ratio_tn, accuratezza))
	f.close()
