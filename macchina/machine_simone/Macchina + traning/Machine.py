#!/bin/python
# -*- coding utf-8 -*-

import spacy
from tweetU import *

#~ Nome del modello da utilizzare e salvare
#~ possono essere anche lo stesso per migliorare
#~ un modello esistente

name_model= "./model"
save_model = "./model_save"


#~ Nomi dei file per il modello: il file per 
#~ l'allenamento del modello, il file per testare
#~ la macchina e il file dove salvare i risultati

training_file = "./dataset3.csv"
test_file = "./test_value.csv"
output_file = "./risultati"
tabella = "./tabella"

def main(New=False):

	data = {
		"training" : training_file,
		"output"   : output_file,
		"test"		 : training_file,
		"tabella"  : tabella,
		"save"		 : save_model
	} 

	if New:
		nlp = spacy.load('it', disable=['ner', 'parser'])
	else:
		nlp = spacy.load(name_model)


	# Carico il dataset
	print ("Carico il dataset\n")
	train_data = load_data(data, nlp)
	
	# Training della macchina
	print ("Inizio l'allenamento\n")
	training(train_data, data, nlp)

	# Test del modello
	print ("Test del modello\n")
	scores = predici(data, nlp)
	
	
	print (len(scores))
	
	# Stampa i risultati
	valuta(data, scores)
	
	
	

main(New=True)
