<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<head>
		<title>Twipy map [CNR IRPI]</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style type="text/css">
			embed { 
				height: 80%;
				width: 95%;
			}
			body {
				background-color: #6d92a0;
			}
		</style>
	</head>
	<body>
		<h1 align="center" style="color:white"> <b>Mappa Tweet</b> </h1>
		<div align="center"><button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-gray"><b>Legenda</b></button><div>

  <div id="id01" class="w3-modal">
    <div class="w3-modal-content">
      <header class="w3-container w3-teal"> 
        <span onclick="document.getElementById('id01').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h2 align="center">Legenda</h2>
      </header>
      <div class="w3-container" align="center">
      <table>
      	<tr>
        <th><span style="color:red; font-size:35px;"><i class="glyphicon glyphicon-map-marker"></i></span></th><th>66% / 100%</th>
        <tr>
        <th><span style="color:orange; font-size:35px;"><i class="glyphicon glyphicon-map-marker"></i></span></th><th>33% / 66%</th>
       <tr>
        <th><span style="color:gray; font-size:35px;"><i class="glyphicon glyphicon-map-marker"></i></span></th><th>0% / 33%</th>
      </table>
      </div>
    </div>
  </div>
</div>
		<div align="center" id="mappa">
			<div>
				<button id="sx" class="w3-button w3-gray">
					<span class="glyphicon glyphicon-chevron-left"></span>
					
				</button>
				<input type="date" id="data" class="w3-border" align="center">
				<button id="dx"class="w3-button w3-gray">
					<span class="glyphicon glyphicon-chevron-right	
"></span>
				</button>
			</div>
			<br>
			<embed id="emb"src="http://194.119.218.92/twitter/mappe/0d" class="w3-border" >
		</div>
		<script>
			var i=0
			var date = document.getElementById("data");
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			if(dd<10) {
			    dd = '0'+dd
			} 
			if(mm<10) {
			    mm = '0'+mm
			} 
			today = yyyy + '-' + mm + '-' + dd;
			date.defaultValue = today;
			date.readOnly = true;
			$(document).ready(function(){
				function move(d){
					var parent = $('#emb').parent();
					var newElement = "<embed src='http://194.119.218.92/twitter/mappe/"+d+"d' id='emb'>";
					$('#emb').remove();
					parent.append(newElement);
				}
				$('#sx').click(function(){
					if (i<8){
						i++;
						move(i);
						date.stepDown()
					}
				});
				$('#dx').click(function(){
					if (i>0){
						i--;
						move(i);
						date.stepUp()
					}
				});
			});
		</script>
	</body>
</html>
