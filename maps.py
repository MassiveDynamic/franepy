#!/usr/bin/env python
# -*- coding: utf-8 -*-
from astral import Astral #libreria per identificare ciclo giorno-notte
import time,datetime,folium
from folium import FeatureGroup, LayerControl, Map, Marker

def sun():
	a=Astral()
	city=a['Rome']
	today_date=datetime.date.today()
	today_hour=int(time.strftime("%H"))*3600
	today_minute=int(time.strftime("%M"))*60
	today_second=int(time.strftime("%S"))
	total_time=today_hour+today_minute+today_second
	sun = city.sun(today_date, local=True)

	#tramonto
	sunset=str(sun['sunset']).split()[1].split('+')[0].split(':')
	total_sunset=int(sunset[0])*3600+int(sunset[1])*60+int(sunset[2])

	#alba
	sunrise=str(sun['sunrise']).split()[1].split('+')[0].split(':')
	total_sunrise=int(sunrise[0])*3600+int(sunrise[1])*60+int(sunrise[2])

	if  not total_time>=total_sunrise and total_time<=total_sunset:
		map_osm = folium.Map(location=[41.903853, 12.484492], zoom_start=6, tiles=None)
		folium.raster_layers.TileLayer(
		    tiles='https://map1.vis.earthdata.nasa.gov/wmts-webmerc/VIIRS_CityLights_2012/default//GoogleMapsCompatible_Level8/{z}/{y}/{x}.jpg',
		    attr='google',
			name='Nasa - Night',
		    max_zoom=20,
		    subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
		    overlay=False,
		    control=True,
		).add_to(map_osm)

		folium.raster_layers.TileLayer(
		    tiles='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
		    attr='google',
		    name='google street view - Day',
		    max_zoom=20,
		    subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
		    overlay=False,
		    control=True,
		).add_to(map_osm)
	else:
		map_osm = folium.Map(location=[41.903853, 12.484492], zoom_start=6, tiles=None)
		folium.raster_layers.TileLayer(
		    tiles='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
		    attr='google',
		    name='google street view - Day',
		    max_zoom=20,
		    subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
		    overlay=False,
		    control=True,
		).add_to(map_osm)

		folium.raster_layers.TileLayer(
			tiles='https://map1.vis.earthdata.nasa.gov/wmts-webmerc/VIIRS_CityLights_2012/default//GoogleMapsCompatible_Level8/{z}/{y}/{x}.jpg',
			attr='google',
			name='Nasa - Night',
			max_zoom=20,
			subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
			overlay=False,
			control=True,
		).add_to(map_osm)
	return map_osm