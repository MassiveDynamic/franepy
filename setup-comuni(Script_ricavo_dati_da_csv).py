# -*- coding: utf-8 -*-
import psycopg2,csv,re
from geopy.geocoders import Nominatim

comuni=[]
cid=[]
lat=[]
lon=[]
gloc = Nominatim(user_agent="CNR")

def main():
	c=0
	with open('comuni.csv', 'r') as csvfile:
		tmp = csv.reader(csvfile, delimiter='\t')
		for row in tmp:
			comuni.append(re.sub(r'[^\x00-\x7F]+',' ',row[1]))
			cid.append(row[0])
	for comune in comuni:
		loc = gloc.geocode(comune)
		lon.append(loc.longitude)
		lat.append(loc.latitude)
		print(str(c)+'>',comuni[c],lat[c],lon[c])
		c=c+1

main()